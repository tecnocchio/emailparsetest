# Email Parse Test

This project is a Spring Boot application designed to parse emails. It demonstrates the use of Java 17, Maven, and Docker to build and run the application.

## Prerequisites

- Java 17
- Maven 3.8.6 or higher
- Docker

## Getting Started

### Building the Project

To build the project using Maven, navigate to the project directory and run the following command:

```sh
mvn clean package
```
## Run application

```sh
mvn spring-boot:run
```

## Testing

This project uses unit tests to ensure the application functions as expected and integration tests to verify the interactions between components. Follow these steps to run the tests:

### Running Unit Tests

To execute the unit tests included with the project, use the following Maven command:

```sh
mvn test
```

## Deployment

This section covers the steps needed to deploy the Email Parse Test application to a production environment using Docker.

### Building the Docker Image

First, build the Docker image with the following command:

```sh
docker build -t email-parse-test:latest .
```
### Running the Application in Production
To run your application in a production environment, you can use Docker. Here's how to start your application using Docker:

```sh
docker run -d -p 80:8080 --name email-app email-parse-test:latest
```

## Authors and acknowledgment
Tecnocchio

## License

This project is licensed under the GNU General Public License v3.0 (GPL-3.0), which allows free use, modification, and distribution under certain conditions.

### Overview of the GPL-3.0 License

The GPL-3.0 is a copyleft license that mandates that any derivative work must also be released under the same license when distributed. This means that any source code that uses or modifies this project must also be made available under the GPL-3.0.

### Key Features of GPL-3.0:

- **Freedom to Run the Program**: You can run the program for any purpose without restrictions.
- **Freedom to Study and Modify the Program**: You have access to the source code and the freedom to modify it.
- **Freedom to Distribute Copies**: You can distribute copies of the original program to others.
- **Freedom to Distribute Modified Versions**: If you modify the program, you can distribute the modified version under the same terms.

### Full License Text

For more details on the GNU General Public License v3.0, please read the full license [here](https://www.gnu.org/licenses/gpl-3.0.en.html). A copy of the GPL-3.0 license is included in the `LICENSE` file in this repository.

### Using This Project Under GPL-3.0

To use this software under the GPL-3.0 license, include a copy of the license in any distributions or derivative works and clearly state any modifications made. It is also recommended to link back to the original source when feasible.

### Contributing to This Project

If you contribute to this project, you are agreeing to share your contributions under the same GPL-3.0 license. This ensures that all contributions remain free and open.

### Contact Information

For any inquiries regarding the use of this project under its license, or to request a different licensing arrangement, please contact me (tecnocchio).

