package org.tecnocchio.emailprocessing.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.tecnocchio.emailprocessing.model.JobStatus;
import org.tecnocchio.emailprocessing.service.CacheService;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class JobServiceImplTest {

    @Mock
    private CacheService cacheService;

    @InjectMocks
    private JobServiceImpl jobService;

    /**
     * Test to ensure that the job status is correctly identified as INVALID when the job data is null.
     * Null data typically indicates an uninitialized or absent job record.
     */
    @Test
    void getJobStatus_ReturnsInvalid_WhenDataIsNull() {
        assertEquals(JobStatus.INVALID, jobService.getJobStatus(null));
    }

    /**
     * Test to verify that the job status returns INVALID when the provided job data explicitly
     * indicates an error condition.
     */
    @Test
    void getJobStatus_ReturnsInvalid_WhenDataIndicatesError() {
        assertEquals(JobStatus.INVALID, jobService.getJobStatus("ERROR_STATUS"));
    }

    /**
     * Test to confirm that the job status is properly returned as IN_PROGRESS when the job data
     * indicates that processing is currently ongoing.
     */
    @Test
    void getJobStatus_ReturnsInProgress_WhenDataIndicatesProcessing() {
        assertEquals(JobStatus.IN_PROGRESS, jobService.getJobStatus("PROCESSING_STATUS"));
    }

    /**
     * Test to ensure that the job status is correctly returned as COMPLETED when the job data
     * does not match known error or processing statuses, suggesting the job has been completed.
     */
    @Test
    void getJobStatus_ReturnsCompleted_WhenDataIsOther() {
        assertEquals(JobStatus.COMPLETED, jobService.getJobStatus("COMPLETED_STATUS"));
    }

}
