package org.tecnocchio.emailprocessing.service.impl.engine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.tecnocchio.emailprocessing.config.ConfigProperties;
import org.tecnocchio.emailprocessing.service.CacheService;

import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileProcessingServiceImplTest {

    public static final String EXAMPLE_COM_VALID = "email,rule\nemail@example.com,Valid\nemail2example.com,Invalid";
    public static final String EMAIL_INVALID = "not_an_email,Invalid\nalso_not_email,Invalid";

    @Mock
    private CacheService cacheService;
    @Mock
    private ConfigProperties configProperties;
    private FileProcessingServiceImpl fileProcessingService;

    @BeforeEach
    void setUp() {
        // Initialize mock to return specific regex and end-of-line characters before each test
        when(configProperties.getEmailRegex()).thenReturn("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
        when(configProperties.getEndOfLine()).thenReturn("\n");

        // Create an instance of the service to be tested with the mocked dependencies
        fileProcessingService = new FileProcessingServiceImpl(cacheService, configProperties);
    }

    @Test
    void testProcessValidFile() throws Exception {
        // Create a mock file with predefined content simulating a CSV with valid and invalid emails
        final MultipartFile file = new MockMultipartFile("file", "test.csv", "text/csv", EXAMPLE_COM_VALID.getBytes());

        final String uniqueId = "unique123";

        // Process the file and verify that caching behavior is as expected for valid data
        fileProcessingService.processFile(file, uniqueId);

        // Assert that cacheData was called once with a result indicating that valid emails were processed
        verify(cacheService, times(1)).cacheData(eq(uniqueId), contains("true"));
    }

    @Test
    void testProcessFileIOException() throws Exception {
        // Create a mock file that throws an IOException to simulate read failure
        final MultipartFile file = mock(MultipartFile.class);
        when(file.getInputStream()).thenThrow(new IOException("Failed to read file"));

        final String uniqueId = "unique125";

        // Process the file and verify caching of an error status due to the IOException
        fileProcessingService.processFile(file, uniqueId);

        // Assert that cacheData was called once with an error status, indicating processing failed
        verify(cacheService, times(1)).cacheData(eq(uniqueId), eq("ERROR_STATUS"));
    }

    @Test
    void testProcessFileWithInvalidData() throws Exception {
        // Create a mock file with invalid email data to test email validation
        final MultipartFile file = new MockMultipartFile("file", "invalid.csv", "text/csv", EMAIL_INVALID.getBytes());

        final String uniqueId = "unique126";

        // Process the file and verify that caching behavior is as expected for invalid data
        fileProcessingService.processFile(file, uniqueId);

        // Assert that cacheData was called once with a result indicating that no valid emails were found
        verify(cacheService, times(1)).cacheData(eq(uniqueId), contains("false"));
    }
}
