package org.tecnocchio.emailprocessing.service.impl.data.memory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.tecnocchio.emailprocessing.service.PersistService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HashMapCacheServiceImplTest {

    @Mock
    private PersistService persistService;

    @InjectMocks
    private HashMapCacheServiceImpl cacheService;

    @BeforeEach
    void setUp() throws Exception {
        cacheService = new HashMapCacheServiceImpl(persistService);
    }

    /**
     * Tests that data is correctly cached and retrievable.
     */
    @Test
    void cacheDataAndRetrieve_Success() throws Exception {
        final String key = "testKey";
        final String value = "testData";

        // Simulate caching the data
        cacheService.cacheData(key, value);

        // Now retrieve using the service method
        final String cachedValue = cacheService.getData(key);

        // Verify internal interactions
        verify(persistService, never()).readFile(key); // Ensure we do not fall back to persistent storage
        assertEquals(value, cachedValue, "Cached data should match the original value");
    }


    @Test
    void testCacheDataCallsPersist() throws Exception {
        final String key = "key";
        final String value = "value";

        // Call the method that invokes the void method
        cacheService.cacheData(key, value);

        // Verify that persistFile was called with the correct parameters
        verify(persistService).persistFile(key, value);
    }

    /**
     * Test retrieving data that has not been cached should return null.
     */
    @Test
    void retrieveNonExistentData_ReturnsNull() throws Exception {
        final String nonExistentKey = "nonExistentKey";

        // Try retrieving data that hasn't been cached
        assertNull(cacheService.getData(nonExistentKey), "Retrieving non-existent data should return null");
        // File is attempted from disk
        verify(persistService, times(1)).readFile(nonExistentKey); // Ensure we do not fall back to persistent storage

    }

    /**
     * Test verifying that if the data is not in the cache, it tries to retrieve it from persistent storage.
     */
    @Test
    void getData_FromPersistService_IfNotInCache() throws Exception {
        final String key = "persistedKey";
        final String value = "persistedData";

        // Assume the data is not in the cache but is in persistent storage
        when(persistService.readFile(key)).thenReturn(value);

        // Retrieve data
        final String retrievedValue = cacheService.getData(key);

        // Verify it fetches from the persistent service if not available in cache
        verify(persistService, times(1)).readFile(key);
        assertEquals(value, retrievedValue, "Should retrieve data from persistent storage when not in cache");
    }
}
