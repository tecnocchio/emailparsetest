package org.tecnocchio.emailprocessing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

//	@Test
	void contextLoads() {
	}

//	@Autowired
	private TestRestTemplate restTemplate;

//	@Test
	void homeResponse() {
		String body = "Spring is here!";
		assertEquals("Spring is here!", body);
	}
}
