package org.tecnocchio.emailprocessing.controller;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;
import org.tecnocchio.emailprocessing.service.FileProcessingService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.tecnocchio.emailprocessing.config.Messages.EMPTY_FILE_UPLOADED;

@WebMvcTest(FileUploadController.class)
public class FileUploadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileProcessingService fileProcessingService;
    @Captor
    private ArgumentCaptor<String> uniqueIdCaptor;
    @Captor
    private ArgumentCaptor<MultipartFile> fileCaptor;

    @Test
    public void whenEmptyFile_thenBadRequest() throws Exception {
        final MockMultipartFile emptyFile = new MockMultipartFile("file", "filename.txt", "text/plain", new byte[0]);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/API/upload").file(emptyFile).contentType(MediaType.MULTIPART_FORM_DATA)).andExpect(status().isBadRequest()).andExpect(content().string(EMPTY_FILE_UPLOADED));
    }

    @Test
    public void whenValidFile_thenProcessFile() throws Exception {
        final MockMultipartFile validFile = new MockMultipartFile("file", "filename.txt", "text/plain", "Some content".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/API/upload").file(validFile).contentType(MediaType.MULTIPART_FORM_DATA)).andExpect(status().isOk()).andExpect(jsonPath("$.id").exists());

        verify(fileProcessingService, times(1)).processFile(fileCaptor.capture(), uniqueIdCaptor.capture());

        // Additional assertions can be added here if needed
        assertEquals(validFile, fileCaptor.getValue());
        assertTrue(uniqueIdCaptor.getValue().matches("[a-f0-9\\-]{36}"));  // Check if the captured uniqueId looks like a UUID
    }
}
