/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Service interface for processing files uploaded to the system.
 * <p>
 * This interface defines the contract for services that handle the asynchronous processing
 * of files, which might include validation, storage, and additional business logic.
 * </p>
 */
public interface FileProcessingService {

    /**
     * Processes the uploaded file asynchronously, performing tasks such as validation, parsing,
     * and storage based on the content of the file.
     * <p>
     * Implementations should ensure that the file content is processed in accordance with
     * application-specific requirements. This process might involve extracting data, validating
     * formats, and storing information in databases or other storage systems.
     * </p>
     * <p>
     * The process should handle any errors internally and may log or communicate these as needed.
     * Clients of this service should be aware that the processing is done asynchronously and
     * completion of this method does not imply completion of the processing.
     * </p>
     *
     * @param file     the uploaded file to be processed. Must not be {@code null}.
     * @param uniqueId a unique identifier for the file processing session. This ID can be used
     *                 to track the progress or result of the processing. Must not be {@code null}.
     * @throws IllegalArgumentException if the file or uniqueId is {@code null}, or if the file
     *                                  content does not meet the expected format or criteria.
     */
    void processFile(MultipartFile file, String uniqueId);
}
