/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service;

import org.tecnocchio.emailprocessing.model.JobStatus;

/**
 * Interface defining the contract for services that manage and retrieve job status and data.
 * <p>
 * This interface allows for querying job status and fetching job-specific data, facilitating
 * the monitoring and management of job execution within the application.
 * </p>
 */
public interface JobService {

    /**
     * Retrieves the status of a job based on provided job data.
     * <p>
     * This method is responsible for determining the status of a job by interpreting
     * its associated data, which could be indicative of its current state in a processing workflow.
     * </p>
     *
     * @param jobData A string representation of job-specific data, typically an identifier or status code.
     * @return The {@link JobStatus} representing the current state of the job.
     *         Returns {@link JobStatus#INVALID} if the jobData does not correspond to a valid job.
     */
    JobStatus getJobStatus(String jobData);

    /**
     * Fetches the data associated with a specific job identifier.
     * <p>
     * This method retrieves detailed information or data related to a specific job, identified by a unique ID.
     * The nature of the data returned can vary depending on the job's type and the stage of its processing.
     * </p>
     *
     * @param id The unique identifier for the job whose data is being requested.
     * @return A string containing the data related to the job, or {@code null} if no data is available for the given ID.
     */
    String getJobData(String id);
}
