/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service;

import java.io.IOException;

/**
 * Interface defining the contract for persistent file storage and retrieval services.
 * <p>
 * This interface provides methods for storing and retrieving data to and from persistent storage,
 * encapsulating the underlying implementation details such as file handling and I/O operations.
 * </p>
 */
public interface PersistService {

    /**
     * Stores content in a persistent storage medium under the specified file name.
     * <p>
     * This method should ensure that the content is written to the storage in a durable manner.
     * Implementations must handle any I/O exceptions that may occur and ensure data integrity during write operations.
     * </p>
     *
     * @param fileName The name of the file in which to store the content. Must not be {@code null}.
     * @param content The content to be written to the file. Must not be {@code null}.
     * @throws IOException If an I/O error occurs during writing to the file.
     */
    void persistFile(String fileName, String content) throws IOException;

    /**
     * Retrieves content from persistent storage based on the specified file name.
     * <p>
     * This method reads the content of a specified file from storage. Implementations should handle
     * I/O exceptions and may return {@code null} if the file does not exist or if reading is not possible.
     * </p>
     *
     * @param fileName The name of the file to retrieve. Must not be {@code null}.
     * @return The content of the file as a String, or {@code null} if the file cannot be read or does not exist.
     * @throws IOException If an I/O error occurs during reading from the file.
     */
    String readFile(String fileName) throws IOException;
}
