/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service;

/**
 * Provides an interface for caching services.
 * <p>
 * This interface defines methods for storing and retrieving data from a cache.
 * Implementations should ensure that data is stored and retrieved efficiently and safely.
 * </p>
 */
public interface CacheService {

    /**
     * Caches the specified value under the given key.
     * <p>
     * This method should store the value in such a way that it can be efficiently retrieved
     * using the key.
     * </p>
     *
     * @param key   the key under which the value is stored
     * @param value the value to be cached
     */
    void cacheData(String key, String value);

    /**
     * Retrieves the value associated with the given key from the cache.
     * <p>
     * If the key does not exist in the cache, implementations should return {@code null} or
     * handle the case appropriately.
     * </p>
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key, or {@code null} if the key doesn't exist
     */
    String getData(String key);
}
