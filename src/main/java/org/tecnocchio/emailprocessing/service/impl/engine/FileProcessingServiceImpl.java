/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service.impl.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tecnocchio.emailprocessing.config.ConfigProperties;
import org.tecnocchio.emailprocessing.service.CacheService;
import org.tecnocchio.emailprocessing.service.FileProcessingService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

import static org.tecnocchio.emailprocessing.model.ProcessingStatus.ERROR_STATUS;
import static org.tecnocchio.emailprocessing.model.ProcessingStatus.PROCESSING_STATUS;

/**
 * Service implementation for processing files containing email data.
 * <p>
 * This service processes files asynchronously, validates email formats within the file,
 * and caches the processing status and content.
 * </p>
 */
@Service
public class FileProcessingServiceImpl implements FileProcessingService {
    private static final Logger log = LoggerFactory.getLogger(FileProcessingServiceImpl.class);

    private final Pattern EMAIL_REGEX;  // Regex to validate email formats.
    private final String LF;  // Line separator for the system.

    private final CacheService cacheService;

    /**
     * Constructs a new {@link FileProcessingServiceImpl} with dependency injection.
     *
     * @param cacheService The service for caching file processing results.
     * @param configProperties Configuration properties that include email regex and line formatting.
     */
    @Autowired
    public FileProcessingServiceImpl(CacheService cacheService, ConfigProperties configProperties) {
        this.cacheService = cacheService;
        this.EMAIL_REGEX = Pattern.compile(configProperties.getEmailRegex());
        this.LF = configProperties.getEndOfLine();
    }

    /**
     * Processes the uploaded file asynchronously.
     *
     * @param file The multi-part file to be processed.
     * @param uniqueId A unique identifier for the file session.
     */
    @Override
    @Async("taskExecutor")
    public void processFile(MultipartFile file, String uniqueId) {
        cacheService.cacheData(uniqueId, PROCESSING_STATUS.name());
        StringBuilder fileContent = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line = reader.readLine();
            if (line == null || line.isEmpty()) {
                log.info("Empty header or file");
            } else {
                fileContent.append(line).append(",validation").append(LF);
            }
            while ((line = reader.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }
                String validation = processLine(line);
                fileContent.append(line).append(",").append(validation).append(LF);
            }
        } catch (IOException e) {
            log.error("Error processing the file {}", uniqueId, e);
            cacheService.cacheData(uniqueId, ERROR_STATUS.name());
            return;
        }
        cacheService.cacheData(uniqueId, fileContent.toString());
    }

    /**
     * Validates a single line from the file for proper email format.
     *
     * @param line The line of text to validate.
     * @return "true" if any csv part of the line matches the email regex, "false" otherwise.
     */
    private String processLine(String line) {
        return Pattern.compile(",")
                .splitAsStream(line)
                .anyMatch(part -> EMAIL_REGEX.matcher(part.trim()).matches()) ? "true" : "false";
    }

}
