/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service.impl.engine;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Configuration for setting up an asynchronous task executor.
 * <p>
 * This configuration class defines a {@link ThreadPoolTaskExecutor} that is used to execute tasks asynchronously.
 * It reads its configuration properties from the application.properties file.
 * </p>
 */
@Configuration
@EnableAsync  // Enables Spring's asynchronous method execution capability
public class AsyncConfig {

    @Value("${task.executor.core.pool.size}")
    private int corePoolSize;  // The core number of threads.

    @Value("${task.executor.max.pool.size}")
    private int maxPoolSize;  // The maximum number of threads.

    @Value("${task.executor.queue.capacity}")
    private int queueCapacity;  // The queue capacity for before thread pool is fully utilized.

    @Value("${task.executor.thread.name.prefix}")
    private String threadNamePrefix;  // The prefix to be used for the names of worker threads.

    /**
     * Defines the bean for task execution.
     * <p>
     * This bean configures the thread pool settings for executing asynchronous tasks.
     * </p>
     * @return the {@link Executor} that will handle asynchronous task execution.
     */
    @Bean(name = "taskExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);  // Set the number of threads that are always kept in the pool.
        executor.setMaxPoolSize(maxPoolSize);  // Set the maximum number of threads.
        executor.setQueueCapacity(queueCapacity);  // Set the capacity of the queue used for holding tasks before they are executed.
        executor.setThreadNamePrefix(threadNamePrefix);  // Set the prefix of every thread's name.
        executor.initialize();  // Initialize the executor to apply the settings.
        return executor;
    }
}
