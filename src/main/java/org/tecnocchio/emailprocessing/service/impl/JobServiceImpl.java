/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tecnocchio.emailprocessing.model.JobStatus;
import org.tecnocchio.emailprocessing.service.CacheService;
import org.tecnocchio.emailprocessing.service.JobService;

import static org.tecnocchio.emailprocessing.model.ProcessingStatus.ERROR_STATUS;
import static org.tecnocchio.emailprocessing.model.ProcessingStatus.PROCESSING_STATUS;

/**
 * Service implementation for managing job statuses and job data retrieval.
 * <p>
 * This service utilizes caching to fetch and interpret job data, determining the status of jobs.
 * </p>
 */
@Service
public class JobServiceImpl implements JobService {

    private final CacheService cacheService;

    /**
     * Constructs a JobServiceImpl with a CacheService.
     *
     * @param cacheService the service responsible for data caching and retrieval
     */
    @Autowired
    public JobServiceImpl(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    /**
     * Determines the status of a job based on its associated data.
     * <p>
     * The status is determined by the string value associated with job data:
     * - If the data is {@code null} or matches {@code ERROR_STATUS}, the job status is {@code INVALID}.
     * - If the data matches {@code PROCESSING_STATUS}, the job status is {@code IN_PROGRESS}.
     * - Otherwise, the job is considered {@code COMPLETED}.
     * </p>
     *
     * @param jobData the cached data associated with a job
     * @return the status of the job
     */
    @Override
    public JobStatus getJobStatus(String jobData) {
        if (jobData == null || ERROR_STATUS.name().equals(jobData)) {
            return JobStatus.INVALID;
        }
        if (PROCESSING_STATUS.name().equals(jobData)) {
            return JobStatus.IN_PROGRESS;
        }
        return JobStatus.COMPLETED;
    }

    /**
     * Retrieves job data from the cache using a unique identifier.
     *
     * @param id the unique identifier for the job
     * @return the data associated with the job, or {@code null} if no data is found
     */
    @Override
    public String getJobData(String id) {
        return cacheService.getData(id);
    }
}
