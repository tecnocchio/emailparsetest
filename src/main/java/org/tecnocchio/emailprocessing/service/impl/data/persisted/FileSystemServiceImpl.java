/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service.impl.data.persisted;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tecnocchio.emailprocessing.config.ConfigProperties;
import org.tecnocchio.emailprocessing.service.PersistService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Service implementation for file persistence operations.
 *
 * This service handles the persistence and retrieval of files using the local filesystem.
 * It utilizes a concurrent map to manage file locks, ensuring thread-safe read and write operations.
 */
@Service
public class FileSystemServiceImpl implements PersistService {
    private static final Logger log = LoggerFactory.getLogger(FileSystemServiceImpl.class);

    private final ConcurrentHashMap<String, ReentrantLock> locks = new ConcurrentHashMap<>();
    private final String storagePath;

    /**
     * Constructs a new FileSystemServiceImpl with a specific storage path set through configuration.
     *
     * @param fileStorageProperties Configuration properties that include the storage path.
     */
    @Autowired
    public FileSystemServiceImpl(ConfigProperties fileStorageProperties) {
        this.storagePath = fileStorageProperties.getStoragePath();
    }

    /**
     * Persists content to a file, ensuring thread safety using a reentrant lock.
     *
     * @param fileName the name of the file to write to.
     * @param content the content to write to the file.
     * @throws IOException if an I/O error occurs writing to or creating the file.
     */
    @Override
    public void persistFile(String fileName, String content) throws IOException {
        final ReentrantLock lock = locks.computeIfAbsent(fileName, k -> new ReentrantLock());
        lock.lock();
        try {
            final Path filePath = Paths.get(storagePath, fileName);
            log.debug("Writing to file: {}", filePath);
            Files.createDirectories(filePath.getParent());
            Files.write(filePath, content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.error("Error while writing the file {}", fileName, e);
            throw e;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Reads the content of a file, ensuring thread safety using a reentrant lock.
     *
     * @param fileName the name of the file to read.
     * @return the content of the file as a String, or null if the file does not exist.
     * @throws IOException if an I/O error occurs during reading the file.
     */
    @Override
    public String readFile(String fileName) throws IOException {
        String value = null;
        final ReentrantLock lock = locks.computeIfAbsent(fileName, k -> new ReentrantLock());
        lock.lock();
        try {
            final Path filePath = Paths.get(storagePath, fileName);
            log.debug("Reading from file: {}", filePath);
            if (Files.exists(filePath)) {
                value = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            log.error("Error reading file {}", fileName, e);
            throw e;
        } finally {
            lock.unlock();
        }
        return value;
    }
}
