/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.service.impl.data.memory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tecnocchio.emailprocessing.service.CacheService;
import org.tecnocchio.emailprocessing.service.PersistService;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Service implementation for caching and persisting data using a hash map and a file system.
 * <p>
 * This service provides caching capabilities with thread-safe access and persistence backup
 * via an injected {@link PersistService}. Data is initially stored in memory and optionally
 * persisted to disk for durability.
 * </p>
 */
@Service
public class HashMapCacheServiceImpl implements CacheService {

    private static final Logger log = LoggerFactory.getLogger(HashMapCacheServiceImpl.class);

    private final Map<String, String> cache = new ConcurrentHashMap<>();
    private final PersistService fileSystemService;

    /**
     * Creates an instance of {@code HashMapCacheServiceImpl} with a dependency on {@link PersistService}.
     *
     * @param fileSystemService the service responsible for persisting data to the file system
     */
    @Autowired
    public HashMapCacheServiceImpl(PersistService fileSystemService) {
        this.fileSystemService = fileSystemService;
    }

    /**
     * Stores the given value associated with the specified key in the cache and persists it asynchronously.
     *
     * @param key   the key with which the specified value is to be associated
     * @param value the value to be associated with the specified key
     */
    @Override
    public void cacheData(String key, String value) {
        cache.put(key, value);
        try {
            fileSystemService.persistFile(key, value);
        } catch (IOException e) {
            log.error("Failed to persist data for key {}: {}", key, e.getMessage());
        }
    }

    /**
     * Retrieves the value associated with the specified key from the cache.
     * If the value is not present in the cache, it attempts to load it from persistent storage.
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key, or {@code null} if no value is found
     */
    @Override
    public String getData(String key) {
        String value = cache.get(key);
        if (value == null) {
            try {
                value = fileSystemService.readFile(key);
                if (value != null) {
                    cache.put(key, value);
                }
            } catch (IOException e) {
                log.error("Failed to read data for key {}: {}", key, e.getMessage());
            }
        }
        return value;
    }
}
