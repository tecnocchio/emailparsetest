/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.exception;

/**
 * Custom exception class for handling file-related errors.
 * <p>
 * This class extends {@link Exception} and provides additional constructors
 * for specifying error messages and nested exceptions.
 * </p>
 */
public class FileException extends Exception {

    /**
     * Constructs a new {@code FileException} with the specified detail message.
     *
     * @param message the detail message
     */
    public FileException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@code FileException} with no detail message.
     */
    public FileException() {
        super();
    }

    /**
     * Constructs a new {@code FileException} with the specified detail message
     * and cause.
     *
     * @param message the detail message
     * @param e the cause of the exception
     */
    public FileException(String message, Exception e) {
        super(message, e);
    }

    /**
     * Returns a string representation of the {@code FileException}.
     * <p>
     * The string representation contains the exception class name and the detail message.
     * </p>
     *
     * @return a string representation of the {@code FileException}
     */
    @Override
    public String toString() {
        return "FileException: " + getMessage();
    }
}
