/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tecnocchio.emailprocessing.service.FileProcessingService;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.tecnocchio.emailprocessing.config.Messages.EMPTY_FILE_UPLOADED;

/**
 * REST controller for handling file upload requests.
 * <p>
 * This controller provides an endpoint for uploading files. The uploaded files
 * are processed asynchronously.
 * </p>
 */
@RestController
public class FileUploadController {

    private final FileProcessingService fileProcessingService;

    /**
     * Constructs a new {@code FileUploadController} with the given {@code FileProcessingService}.
     *
     * @param fileProcessingService the file processing service
     */
    @Autowired
    public FileUploadController(FileProcessingService fileProcessingService) {
        this.fileProcessingService = fileProcessingService;
    }

    /**
     * Handles the file upload request.
     * <p>
     * This method processes the uploaded file asynchronously. If the file is empty,
     * it returns a bad request response. Otherwise, it returns a response with a unique ID
     * for tracking the file processing status.
     * </p>
     *
     * @param file the file to be uploaded
     * @return a {@code ResponseEntity} containing the response status and body
     */
    @PostMapping("/API/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(EMPTY_FILE_UPLOADED);
        }
        // Generate unique ID
        final String uniqueId = UUID.randomUUID().toString();

        // Call async method to process file
        fileProcessingService.processFile(file, uniqueId);

        // Return the response
        final Map<String, String> response = new HashMap<>();
        response.put("id", uniqueId);

        return ResponseEntity.ok(response);
    }
}
