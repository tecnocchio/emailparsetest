/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tecnocchio.emailprocessing.model.JobStatus;
import org.tecnocchio.emailprocessing.service.JobService;

import java.nio.charset.StandardCharsets;

import static org.tecnocchio.emailprocessing.config.Messages.ERROR_INVALID_JOB_ID;
import static org.tecnocchio.emailprocessing.config.Messages.ERROR_JOB_IS_IN_PROGRESS;

/**
 * REST controller for handling job-related requests.
 * <p>
 * This controller provides an endpoint for downloading job data based on the job ID.
 * </p>
 */
@RestController
@RequestMapping("/API")
public class JobController {

    private final JobService jobService;

    /**
     * Constructs a new {@code JobController} with the given {@code JobService}.
     *
     * @param jobService the job service
     */
    @Autowired
    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    /**
     * Handles the request to download job data.
     * <p>
     * This method retrieves the job data and its status based on the job ID.
     * If the job is completed, it returns the data as a downloadable attachment.
     * If the job is in progress, it returns a locked status.
     * If the job ID is invalid, it returns a bad request status.
     * </p>
     *
     * @param id the job ID
     * @return a {@code ResponseEntity} containing the response status and body
     */
    @GetMapping("/download/{id}")
    public ResponseEntity<?> downloadJobData(@PathVariable String id) {
        final String job = jobService.getJobData(id);
        final JobStatus jobStatus = jobService.getJobStatus(job);

        switch (jobStatus) {
            case COMPLETED:
                return ResponseEntity.ok()
                        .header("Content-Disposition", "attachment; filename=\"" + id + ".blob\"")
                        .body(job.getBytes(StandardCharsets.UTF_8));

            case IN_PROGRESS:
                return ResponseEntity.status(HttpStatus.LOCKED).body(ERROR_JOB_IS_IN_PROGRESS);

            case INVALID:
            default:
                return ResponseEntity.badRequest().body(ERROR_INVALID_JOB_ID);
        }
    }
}
