/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * The entry point of the Spring Boot application.
 * <p>
 * This class serves as the launching point for starting the Spring Boot application. It is
 * annotated with {@link SpringBootApplication}, which enables Spring Boot's auto-configuration,
 * component scan, and additional configuration capabilities. {@link EnableAsync} is used to
 * allow asynchronous method execution within the application.
 * </p>
 */
@SpringBootApplication  // Enable auto-configuration, component scanning, and additional configurations.
@EnableAsync            // Enable support for asynchronous methods.
public class BootApplication {

    /**
     * Main method to run the Spring Boot application.
     * <p>
     * This is the entry point used by the JVM to start the application. It delegates to
     * Spring Boot's {@link SpringApplication#run} method to bootstrap the application.
     * </p>
     *
     * @param args Command-line arguments passed to the application.
     */
    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }
}
