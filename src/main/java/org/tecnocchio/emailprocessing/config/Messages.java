/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.config;

/**
 * Contains constant messages used for various application responses.
 * <p>
 * This class holds the constant string messages that are used throughout
 * the application for indicating different statuses or errors.
 * </p>
 */
public class Messages {

    /**
     * Message indicating that a job is currently in progress.
     */
    public static final String ERROR_JOB_IS_IN_PROGRESS = "Job is in progress";

    /**
     * Message indicating that the provided job ID is invalid.
     */
    public static final String ERROR_INVALID_JOB_ID = "Invalid job ID";

    /**
     * Message indicating that an uploaded file is empty.
     */
    public static final String EMPTY_FILE_UPLOADED = "Empty file uploaded";
}
