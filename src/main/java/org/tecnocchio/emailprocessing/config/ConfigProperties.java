/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration properties for file storage and email processing.
 * <p>
 * This class holds the configuration properties for the application, such as
 * the file storage path, end-of-line character, and email regex pattern. The
 * properties are injected from the application properties file.
 * </p>
 */
@Setter
@Getter
@Configuration
public class ConfigProperties {

    /**
     * The path where files are stored.
     * <p>
     * This property is injected from the application properties file using the
     * key <code>file.storage.path</code>.
     * </p>
     */
    @Value("${file.storage.path}")
    private String storagePath;

    /**
     * The end-of-line character used in files.
     * <p>
     * This property is injected from the application properties file using the
     * key <code>file.end.of.line</code>.
     * </p>
     */
    @Value("${file.end.of.line}")
    private String endOfLine;

    /**
     * The regular expression pattern for validating email addresses.
     * <p>
     * This property is injected from the application properties file using the
     * key <code>email.regex</code>.
     * </p>
     */
    @Value("${email.regex}")
    private String emailRegex;
}
