/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.model;

/**
 * Enum representing the processing status of a job in the email processing application.
 * <p>
 * This enum defines two possible processing statuses:
 * </p>
 * <ul>
 *   <li>{@link #PROCESSING_STATUS}: Indicates that the job is currently being processed.</li>
 *   <li>{@link #ERROR_STATUS}: Indicates that an error occurred during the processing of the job.</li>
 * </ul>
 */
public enum ProcessingStatus {

     /**
      * Indicates that the job is currently being processed.
      */
     PROCESSING_STATUS,

     /**
      * Indicates that an error occurred during the processing of the job.
      */
     ERROR_STATUS
}
