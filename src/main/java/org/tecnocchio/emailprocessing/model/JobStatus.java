/*
 * Copyright (c) 2024 Tecnocchio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.tecnocchio.emailprocessing.model;

/**
 * Represents the status of a job in the email processing application.
 * <p>
 * The job can have one of the following statuses:
 * </p>
 * <ul>
 *   <li>{@link #COMPLETED}: The job has been completed successfully.</li>
 *   <li>{@link #IN_PROGRESS}: The job is currently in progress.</li>
 *   <li>{@link #INVALID}: The job ID provided is invalid.</li>
 * </ul>
 */
public enum JobStatus {
    /**
     * Indicates that the job has been completed successfully.
     */
    COMPLETED,

    /**
     * Indicates that the job is currently in progress.
     */
    IN_PROGRESS,

    /**
     * Indicates that the job ID provided is invalid.
     */
    INVALID
}
